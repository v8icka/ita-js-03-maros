import React from 'react';
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form';

const validate = values => {
    const errors = {};
    if (!values.username) {
        errors.username = 'Required'
    } else if (values.username.length < 4) {
        errors.username = 'Must be 4 characters or more'
    }
    if (!values.password) {
        errors.password = 'Required'
    }
    return errors
};

const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    <div>
        {touched && error && <div className="alert alert-danger">
            <strong>Wrong!</strong> {error}
        </div>}
        <label>{label}</label>
        <div className="form-group">
            <input className="form-control" {...input} placeholder={label} type={type}/>
        </div>
    </div>
);

export class Login extends React.Component {
    render() {
        const {handleSubmit, submitting} = this.props;
        return (
            <div className="container-fluid">
                <div className="row">
                    <h1 className="text-center">Login</h1>
                </div>
                <div className="row">
                    <div className="col-xs-offset-3 col-xs-6">

                        <form onSubmit={handleSubmit}>
                            <Field name="username" type="text" component={renderField} label="Username"/>
                            <Field name="password" type="password" component={renderField} label="Password"/>
                            <div>
                                <button className="btn btn-success" type="submit" disabled={submitting}
                                        onClick={this.props.login}>Login
                                </button>
                                <Link className="btn btn-info main-btn" to='/register'>Go to register</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        )
    }
}

Login = reduxForm({
    form: 'Login',
    validate
})(Login);