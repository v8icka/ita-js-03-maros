import React from 'react';
import Modal from 'react-modal';
import {User} from '../Service/user-service';

export class CustomModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: props.isOpen,
            timeToLoggedOut : 60,
            profile: JSON.parse(props.profile)
        };
    }

    componentWillMount() {
        this.timer();
    }

    timer() {
        window.setInterval(()=>{
            this.setState({
                timeToLoggedOut: this.state.timeToLoggedOut - 1
            });
            if(this.state.timeToLoggedOut === 0){
                this.handleLogout();
            }
        },1000)
    }

    handleLogout() {
        this.loggedOut()
    }

    async loggedOut() {
        await User.logout();
        this.props.loggedIn('');
        window.location.href = "http://localhost:3000/login";
    }

    handleLoggedIn(){
        this.LoginUser();
    };

    async LoginUser() {
        let values = {username: this.state.profile[1].toString(), password:this.state.profile[3].toString()};
        const user = await User.login(values);
        if (user) {
            this.props.loggedIn(user);
            window.location.href = "http://localhost:3000/contact";
        } else {
            console.log("Something wrong on server");
        }
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    render() {

        let customStyle = {
            content: {
                border: 'none',
                background: 'none',
            }
        };

        let buttons = {
            openButton: null,
            actionButton: null
        };

        if (this.props.type === "delete") {
            buttons.openButton = <button className="btn btn-danger main-btn"
                                         onClick={this.openModal.bind(this)}>{this.props.buttonText}</button>
            buttons.actionButton = <div>
                <button onClick={this.closeModal.bind(this)} type="button" className="btn btn-default">
                    Close
                </button>
                <button onClick={this.props.action} type="button"
                        className="btn btn-danger">Delete</button>

            </div>
        }

        if (this.props.type === "sessionExpired") {
            buttons.actionButton = <div>
                <button onClick={this.handleLogout.bind(this)} type="button" className="btn btn-default">
                    Logout
                </button>
                <button onClick={this.handleLoggedIn.bind(this)} type="button"
                        className="btn btn-danger">Stay logged in {this.state.timeToLoggedOut}s</button>

            </div>
        }

        return (

            <div className="main-modal">
                {buttons.openButton}
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.closeModal}
                    contentLabel="custom modal"
                    style={customStyle}
                >

                    <div className="modal-content">
                        <div className="modal-header">
                            <button onClick={this.closeModal.bind(this)} type="button" className="close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title">{this.props.modalTitleText}</h4>
                        </div>
                        <div className="modal-body">
                            <p>{this.props.modalBodyText}</p>
                        </div>
                        <div className="modal-footer">
                            {buttons.actionButton}
                        </div>
                    </div>

                </Modal>
            </div>
        );
    }
}
