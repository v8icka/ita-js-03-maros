import React from 'react';
import {ContactsService} from '../../Service/contact-service';
import {ContactForm} from './contact-form';

export class ContactEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.loadContact();
    }

    async loadContact() {
        const editContact = await ContactsService.getContact(this.props.match.params.id);
        this.setState({
            data: editContact
        })
    }

    handleEdit() {
        this.updateContact();
    }

    async updateContact() {
        await ContactsService.update(this.state.data);
        this.props.history.push(`/contact/detail/${this.state.data.id}`);
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row main-heading">
                    <h1 className="text-center">Edit contact</h1>
                </div>
                <ContactForm handleFormSubmit={this.handleEdit.bind(this)} data={this.state.data}/>
            </div>
        );
    }
};