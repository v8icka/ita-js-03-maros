import React from "react";
import {Link} from 'react-router-dom';
import {ContactListItem} from "./contact-listItem";
import {ContactsService} from '../../Service/contact-service';
import _ from 'lodash';


export class ContactList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    handleTry() {
        this.try()
    }

    async try() {
        await ContactsService.tryCatchUnauthorized();
    }

    componentWillMount() {
        this.loadContacts();
    }

    async loadContacts() {
        const allData = await ContactsService.getContacts();
        this.setState({
            data: allData
        });
    }

    getSearchContact() {
        return _.filter(this.state.data, d=> JSON.stringify(_.values(d)).toLowerCase().includes(this.props.search.toLowerCase()));
    }

    render() {
        const users = this.getSearchContact().map((user) =>

            <ContactListItem key={user.id} url={`/contact/detail/${user.id}`} name={user.name} phone={user.phone}
                             address={user.address}
                             note={user.note}/>
        );

        return (
            <div className="container-fluid">

                <div>
                    <div className="row main-heading">
                        <div className=" col-md-offset-4 col-md-4">
                            <h1 className="text-center">Contacts</h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-6">
                            <Link to="/contact/add">
                                <button className="btn btn-success"> New Contact
                                </button>
                            </Link>
                            <button className="btn btn-warning main-btn" onClick={this.handleTry.bind(this)}>Return 401</button>
                        </div>
                    </div>
                </div>
                <div className="row main-contents">
                    <div className="col-xs-12">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Adress</th>
                                <th>Note</th>
                            </tr>
                            </thead>
                            <tbody>
                            {users}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

