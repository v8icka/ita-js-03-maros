import React from "react";
import {Link} from 'react-router-dom';
import {User} from '../Service/user-service';


export class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            initialValue: props.root.search
        }
    }

    handleChange(e) {
        this.props.search(e.target.value);
    }

    handleLogout() {
        this.logout();
    }

    async logout() {
        await User.logout();
        this.props.loggedOut('');
        window.location.href = "http://localhost:3000/login";
    }

    render() {
        let navigations = null;

        JSON.parse(this.props.root.UserLoggedIn) ? (

            navigations =
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-5">
                    <ul className="nav navbar-nav">
                        <li><Link className="navbar-brand" to={this.props.direction}>App</Link></li>
                        <li><Link to='/contact'>Contact</Link></li>
                        <li><Link to='/contract'>Contract</Link></li>
                    </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <form className="navbar-form navbar-left" role="search">
                            <div className="form-group">
                                <input autoFocus={true} type="text" className="form-control" placeholder="Search"
                                       value={this.state.initialValue} onChange={this.handleChange.bind(this)}/>
                            </div>
                        </form>
                        <li><Link to='/profile'>My profile</Link></li>
                        <li>
                            <a onClick={this.handleLogout.bind(this)} className="navbar-link main-log">
                                {this.props.logData}
                            </a>
                        </li>
                    </ul>
                </div>
        ) : (
            navigations =
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-5">
                    <ul className="nav navbar-nav">
                        <li><Link className="navbar-brand" to={this.props.direction}>App</Link></li>
                    </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <li>
                            <Link className="navbar-link main-log" to={this.props.direction}>{this.props.logData}</Link>
                        </li>
                    </ul>
                </div>
        );
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="collapsed navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-5" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                    </div>
                    {navigations}
                </div>
            </nav>
        )
    };
}
;





