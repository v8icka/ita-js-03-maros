import {createStore, combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

const rootReducer = (state = {
    search: "",
    UserLoggedIn: localStorage.getItem('user'),
    sessionExpired: false
}, action) => {
    switch (action.type) {

        case 'SWITCH_LOGGED_IN':
            localStorage.setItem('user',JSON.stringify(action.payload));
            const user = localStorage.getItem('user');
                state = {
                ...state,
                UserLoggedInID: user
            };
            break;

        case 'SEARCH':
            state = {
                ...state,
                search: action.payload
            };
            break;
        
        case 'SESSION_EXPIRED':
            return Object.assign({}, state, {sessionExpired: true})

        default:
            return state;
    }

    return state;
};

const store = createStore(
    combineReducers({root: rootReducer, form: formReducer})
);

export default store;



