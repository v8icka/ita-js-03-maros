import _ from 'lodash'
import $http from 'axios'

export class ContactsService {

    static async getContacts() {
        return _.values((await $http.get('http://localhost:5000/contacts')).data)
    }

    static async getContact(id) {
        return (await $http.get(`http://localhost:5000/contacts/${id}`)).data
    }

    static async create(data) {
        return (await $http.post(`http://localhost:5000/contacts`, data))
    }

    static async update(data) {
        return (await $http.post(`http://localhost:5000/contacts/:id`, data))
    }

    static async delete(id) {
        return (await $http.delete(`http://localhost:5000/contacts/${id}`))
    }

    static async tryCatchUnauthorized() {
        await $http.get(`http://localhost:5000/tryCatchUnauthorized`);
    }
}